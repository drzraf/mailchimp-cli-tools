## Summary

- mailchimp-user: CRUD a single MailChimp user subscription for a given list.
- mailchimp-summarize: Display MailChimp audience(s) details
- mailchimp-automation: List MailChimp automations for a given account
- member-patcher: Subscriber segment batch transformation tool

`MAILCHIMP_API_KEY` environment variable is expected to contains the API key.

## mailchimp-user
```
usage: mailchimp-user    [-h] -l MCLIST [-v] [-f] [-j | -y | --format FMT]
                         [--status {subscribed,pending,unsubscribed,cleaned}]
                         [--groups GROUPS] [--tags TAGS] [--fields FIELDS]
                         [--language {en,es,pt,de,fr,it}]
                         [--email-type {text,html}]
                         {get,delete,set,md5,rm,ls,patch,post,ins} emails
                         [emails ...]

CRUD a single MailChimp user subscription for a given list.

Example: Be verbose, and just dump the subscribe in Yaml format:
 $ mailchimp-user -vyl 33xxxaxxb0 get test@example.com
Example: Update an subscriber:
 $ mailchimp-user -l 33xxxaxxb0 set test@example.com --email-type html --language fr

positional arguments:
  {get,delete,set,md5,rm,ls,patch,post,ins}
                        Action (`ls`, `set`, `ins` and `rm` are respective aliases for `get`, 'patch`, `post` and `delete`)
  emails                Email

optional arguments:
  -h, --help            show this help message and exit
  -l MCLIST, --list MCLIST
                        The list to search
  -v, --verbose         More verbose
  -f, --force           Force. With rm : delete permanently
  -j, --json            Show user in json format
  -y, --yaml            Show user in yaml format
  --format FMT          Show user using this specific format

Options to `action` == set:
  --status {subscribed,pending,unsubscribed,cleaned}
  --groups GROUPS
  --tags TAGS
  --fields FIELDS
  --language {en,es,pt,de,fr,it}
  --email-type {text,html}
```


## mailchimp-summarize
```
usage: mailchimp-summarize   [-h] [-v] [-s] [-d] [-y] [-T] [-g] [-t] [-i]
                              [-m] [-l FILTER_LIST [FILTER_LIST ...]]
                              [--filter-group FILTER_GROUP] [-f FMT1]
                              [--tag-format TAG_FMT]
                              [--group-format GROUP_FMT]
                              [--interest-format INTEREST_FMT]
                              [--merge-field-format MERGE_FIELD_FMT]

Display MailChimp audience(s) details
Ex: show interest groups for the "Foo bar" list:
$ mailchimp-summarize -igl Foo

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         More verbose
  -s, --stats           Show stats
  -d, --dump            Just dump
  -y, --yaml            Dump as YAML
  -T, --table           Dump as an HTML table
  -g, --groups          Also show groups
  -t, --tags            Also list tags
  -i, --interests       Also show interests
  -m, --merge-fields    Also show merge fields
  -l FILTER_LIST [FILTER_LIST ...], --filter-list FILTER_LIST [FILTER_LIST ...]
                        Filter results to these lists (id or name substring)
  --filter-group FILTER_GROUP
                        Comma-separated list of group to filter (+in/-out)
  -f FMT1, --format FMT1
                        Default display format for first line
  --tag-format TAG_FMT  Default display format for a tag
  --group-format GROUP_FMT
                        Default display format for a group
  --interest-format INTEREST_FMT
                        Default display format for an interest group
  --merge-field-format MERGE_FIELD_FMT
                        Default display format for a merge field

Some other possible list placeholders (among many others):
{subscribe_url_short}, {subscribe_url_long} Possible merge-fields
placeholders: {merge_id}, {tag}, {name}, {type}, {required}, {default_value},
{public}, {display_order}
```

## mailchimp-automation
```
usage: mailchimp-automation    [-h] [-v] [-d] [-e] [-q]
                               [-a FILTER_AUTOMATION [FILTER_AUTOMATION ...]]
                               [-l FILTER_LIST [FILTER_LIST ...]]
                               [-s FILTER_STATUS [FILTER_STATUS ...]]
                               [--filter-email FILTER_EMAIL [FILTER_EMAIL ...]]
                               [-f FMT1] [--email-format EMAIL_FMT]
                               [--queue-format QUEUE_FMT]

List MailChimp automations for a given account
Ex: show emails and possible queued entries for automation(s) matching "*Thank*":
$ mailchimp-automation -qeva Thank

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         More verbose
  -d, --dump            Just dump
  -e, --emails          Also show workflow's emails
  -q, --queue           Also show workflow's emails queue
  -a FILTER_AUTOMATION [FILTER_AUTOMATION ...], --filter-automation FILTER_AUTOMATION [FILTER_AUTOMATION ...]
                        Filter results to these automation (id or name
                        substring)
  -l FILTER_LIST [FILTER_LIST ...], --filter-list FILTER_LIST [FILTER_LIST ...]
                        Filter results to these lists (id or name substring)
  -s FILTER_STATUS [FILTER_STATUS ...], --filter-status FILTER_STATUS [FILTER_STATUS ...]
                        Only present workflows of these status [sending, save,
                        paused]
  --filter-email FILTER_EMAIL [FILTER_EMAIL ...]
                        Filter results to these emails
  -f FMT1, --format FMT1
                        Default display format for automations
  --email-format EMAIL_FMT
                        Default display format for an email
  --queue-format QUEUE_FMT
                        Default display format for a queue entries (if any)
```

## member-patcher
```
$ members-patcher [OPTIONS] [-u] LIST <operation | file> [operation args]
$ members-patcher -t batch_id

* Batch download a (filtered) list of members
* Generates a JSON representing batch operations to apply to each member
* Optionally upload the request and track batch processing status

<operation> will apply in batch to the result set and could be one of:
 * interest <interest id>
 * tag      <tag name>
 * status   <status>
 * rm
 * <any existing regular file>, eg "./file-to-filter.jq"
 * <any json string defining a operation>, eg "{method:\"PATCH\",body: {foo:bar} | @json }"

OPTIONS:
-F fields       Remote API: Fields to fetch from remote API (default: members.id,members.email_address,members.merge_fields.SOURCE)
-p parameters   Remote API: Arbitrary query-string to pass to /members endpoint. Eg: status=subscribed
-f filter_expr  Local filtering (apply to downloaded member list): Any jq expression. Eg: 'select(.merge_fields.FNAME|startswith("Rapha")) | '
-l limit        Limit to the number of results to modify/upload (integer)
-u              Actually *UPLOAD* the batch request. If omitted, the batch is just dumped on stdout.

Other options:
-i              Provide the members as an pure list in stdin. See below example.
-A              Alias for the jq `members` array
-J              Pass --argjson to jq named after "arg"
-d debug_expr   Debugging. Also defines which data to output. Default: [.id,.email_address,.merge_fields.FNAME]
-h              This help

Using members-patcher -t batch_id
-t batch_id     The id of ongoing batch status to request.

Environment:
* DIR                 The directory to upload temporary data to. Default to current directory.
* MAILCHIMP_API_KEY   Mandatory API key

jq variables:
* listid              $MAILCHIMP_API_KEY

Example:
* -p "interest_category_id=bf4d29385b&interest_ids=647bc09890&interest_match=any"

Example:
# After downloading the audience, select subscribers based on the email address suffix and triggers a batch removal.
* jq -rs '[.[] | .members[] | select(.email_address | test("(ee|lv|ru|lt|bg|hr|ro|cz|pl)$"))]' *json | members-patcher -i  rm
```